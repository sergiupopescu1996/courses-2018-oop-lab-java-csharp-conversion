﻿using System;

namespace Unibo.Oop.Utils
{
    public static class Tuple
    {
        public static ITuple<T1> Of<T1>(T1 value1)
        {
            return new TupleImpl<T1>(value1);
        }

        public static ITuple<T1, T2> Of<T1, T2>(T1 value1, T2 value2)
        {
            return new TupleImpl<T1, T2>(value1, value2);
        }

        public static ITuple<T1, T2, T3> Of<T1, T2, T3>(T1 value1, T2 value2, T3 value3)
        {
            return new TupleImpl<T1, T2, T3>(value1, value2, value3);
        }
    }
}
