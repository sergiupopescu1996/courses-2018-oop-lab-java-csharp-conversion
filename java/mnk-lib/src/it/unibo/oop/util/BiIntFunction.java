package it.unibo.oop.util;

@FunctionalInterface
public interface BiIntFunction<R> {
    R apply(int i, int j);
}
