package it.unibo.oop.util;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class ArrayMatrix<E> implements Matrix<E> {

    private final int rows, columns;
    private final List<E> elements;

    public ArrayMatrix(int rows, int columns) {
        this(rows, columns, Collections.emptyList());
    }

    public ArrayMatrix(int rows, int columns, Collection<E> elements) {
        if (rows <= 0 || columns <= 0) {
            throw new IllegalArgumentException();
        }
        this.rows = rows;
        this.columns = columns;
        this.elements = new ArrayList<>(rows * columns);
        this.elements.addAll(elements);
        while (this.elements.size() < this.size()) {
            this.elements.add(null);
        }
    }

    @Override
    public int getColumnsSize() {
        return columns;
    }

    @Override
    public int getRowsSize() {
        return rows;
    }

    protected final int toLinearIndex(int i, int j) {
        return i * columns + j;
    }

    protected final int toColumnIndex(int k) {
        return k % columns;
    }

    protected final int toRowIndex(int k) {
        return k / columns;
    }

    protected final  boolean isInside(int i, int j) {
        return i >= 0 && i < rows && j >= 0 && j < columns;
    }

    protected final void ensureInside(int i, int j) {
        if (!isInside(i, j)){
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public E get(int i, int j) {
        ensureInside(i, j);
        return elements.get(toLinearIndex(i, j));
    }

    @Override
    public Stream<E> getRow(int i) {
        ensureInside(i, 0);
        return IntStream
                .range(0, columns)
                .mapToObj(j -> get(i, j));
    }

    @Override
    public Stream<E> getColumn(int j) {
        ensureInside(0, j);
        return IntStream
                .range(0, rows)
                .mapToObj(i -> get(i, j));
    }

    @Override
    public Stream<E> getDiagonal(int d) {
        if (d >= getColumnsSize() || d <= -getRowsSize()) {
            throw new IndexOutOfBoundsException();
        }
        if (d >= 0) {
            return IntStream
                    .range(0, Math.min(rows, columns))
                    .filter(i -> isInside(i, i + d))
                    .mapToObj(i -> get(i, i + d));
        } else {
            return IntStream
                    .range(0, Math.min(rows, columns))
                    .filter(i -> isInside(i - d, i))
                    .mapToObj(i -> get(i - d, i));
        }
    }

    @Override
    public String toString() {
        return IntStream.range(0, rows)
                .mapToObj(this::getRow)
                .map(s -> s.map(Object::toString).collect(Collectors.joining(", ", "[", "]")))
                .map(Object::toString)
                .collect(Collectors.joining("\n"));
    }

    @Override
    public Stream<E> getAntidiagonal(int a) {
        if (a >= getColumnsSize() || a <= -getRowsSize()) {
            throw new IndexOutOfBoundsException();
        }
        if (a >= 0) {
            return IntStream
                    .range(0, Math.min(rows, columns))
                    .filter(i -> isInside(i, columns - 1 - i - a))
                    .mapToObj(i -> get(i, columns - 1 - i - a));
        } else {
            return IntStream
                    .range(0, Math.min(rows, columns))
                    .filter(i -> isInside(i - a, columns - 1 - i ))
                    .mapToObj(i -> get(i - a, columns - 1 - i ));
        }
    }

    public int size() {
        return columns * rows;
    }

    public boolean contains(E o) {
        return elements.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return elements.iterator();
    }

    public Object[] toArray() {
        return elements.toArray();
    }

    public E[] toArray(E[] ts) {
        return elements.toArray(ts);
    }

    public List<E> toList() {
        return new ArrayList<>(elements);
    }

    @Override
    public int coordDiagonal(int i, int j) {
        final int d = j - i;
        if (d >= getColumnsSize() || d <= -getRowsSize()) {
            throw new IndexOutOfBoundsException();
        }
        return d;
    }

    @Override
    public int coordAntidiagonal(int i, int j) {
        final int a = getColumnsSize() - 1 - j - i;
        if (a >= getColumnsSize() || a <= -getRowsSize()) {
            throw new IndexOutOfBoundsException();
        }
        return a;
    }

    @Override
    public int coordRow(int d, int a) {
        final int i = (getColumnsSize() - 1 - d - a) / 2;
        if (i < 0 || i >= getRowsSize()) {
            throw new IndexOutOfBoundsException();
        }
        return i;
    }

    @Override
    public int coordColumn(int d, int a) {
        final int j = coordRow(d, a) + d;
        if (j < 0 || j >= getColumnsSize()) {
            throw new IndexOutOfBoundsException();
        }
        return j;
    }

    @Override
    public void setDiagonals(int d, int a, E e) {
        final int i = coordRow(d, a);
        final int j = coordColumn(d, a);
        set(i, j, e);
    }

    @Override
    public E getDiagonals(int d, int a) {
        final int i = coordRow(d, a);
        final int j = coordColumn(d, a);
        return get(i, j);
    }

    @Override
    public void forEachIndexed(BiIntObjConsumer<E> consumer)  {
        for (int i = 0; i < getRowsSize(); i++) {
            for (int j = 0; j < getColumnsSize(); j++) {
                consumer.apply(i, j, get(i, j));
            }
        }
    }

    @Override
    public void set(int i, int j, E e) {
        ensureInside(i, j);
        elements.set(toLinearIndex(i, j), e);
    }

    @Override
    public void setAll(E e) {
        for (int i = 0; i < getRowsSize(); i++) {
            for (int j = 0; j < getColumnsSize(); j++) {
                set(i, j, e);
            }
        }
    }

    @Override
    public void setAll(BiIntObjFunction<E, E> f) {
        for (int i = 0; i < getRowsSize(); i++) {
            for (int j = 0; j < getColumnsSize(); j++) {
                set(i, j, f.apply(i, j, get(i, j)));
            }
        }
    }
}
