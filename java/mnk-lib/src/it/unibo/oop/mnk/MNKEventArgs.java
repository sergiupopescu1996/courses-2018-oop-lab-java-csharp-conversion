package it.unibo.oop.mnk;

import it.unibo.oop.util.Tuple;
import it.unibo.oop.util.Tuple2;

import java.util.Objects;

class MNKEventArgs {
    private final MNKMatch source;
    private final int turn;
    private final Symbols player;
    private final Tuple2<Integer, Integer> move;

    public MNKEventArgs(MNKMatch source, int turn, Symbols player, int i, int j) {
        this.source = Objects.requireNonNull(source);
        this.turn = turn;
        this.player = player;
        this.move = Tuple.of(i, j);
    }

    public MNKMatch getSource() {
        return source;
    }

    public int getTurn() {
        return turn;
    }

    public Symbols getPlayer() {
        return player;
    }

    public Tuple2<Integer, Integer> getMove() {
        return move;
    }
}
